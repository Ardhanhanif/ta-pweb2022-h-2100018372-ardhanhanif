<?php 

echo ("<font face=calibri size=3 color=#00BFFF><b>");
echo "NAMA 	: ARDHAN HANIF<br>";
echo "NIM  	: 2100018372<br>";
echo "Kelas : H";
echo "<h1>PWEB9</h1>";
echo "<hr>";

echo ("<font face=times new roman size=3 color=#191970><b>");
echo "<h2>PROG 9.1</h2>";
echo "Uraian Gaji : <br>";
$gaji = 10000000;
$pajak = 0.1; // == 10%
$bonus = 3000000;
$thp = $gaji - ($gaji*$pajak);
$hasil = $thp + $bonus;

echo "Gaji sebelum ditarik Pajak = Rp. $gaji <br>";
echo "Pajak = $pajak/10%<br>";
echo "Bonus = Rp. $bonus<br>";
echo "Gaji bersih = Rp. $thp<br>";
echo "Gaji bersih dan bonus = Rp. $hasil<br>";
echo "<hr>";

echo "<h2>PROG 9.2</h2>";

$a = 5;
$b = 4;

echo "<h3>Oprator Logika</h3>";
echo "a = $a<br>";
echo "b = $b<br>";
echo " $a == $b : ". ($a == $b);
echo "<br>$a != $b : ". ($a != $b);
echo "<br>$a > $b : ". ($a > $b);
echo "<br>$a < $b : ". ($a < $b);
echo "<br>($a == $b) && ($a > $b) : ".(($a != $b) && ($a > $b));
echo "<br>($a == $b) || ($a > $b) : ".(($a != $b) || ($a > $b));
echo "<br>";
echo "<br>";
echo "Jika : <br>";
echo "Hasil : 0 -> False<br>";
echo "Hasil : 1 -> True";
echo "<hr>";

?>